﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
    public class DepreciacionRepository:MasterRepository,InterfaceDepreciacion
    {
        public IEnumerable<Depreciacion> AllDepreciacion(string descripcion)
        {
            var query = from d in Context.Depreciaciones
                        select d;

            if (!string.IsNullOrEmpty(descripcion))
            {
                query = from d in query
                        where d.Descripcion.ToUpper().Contains(descripcion.ToUpper())
                        select d;
            }
            return query;
        }

        public void Store(Depreciacion depreciacion)
        {
            Context.Depreciaciones.Add(depreciacion);
            Context.SaveChanges();
        }

        public Depreciacion Find(int id)
        {
            var result = from d in Context.Depreciaciones
                         where d.IdDepreciacion == id
                         select d;
            return result.FirstOrDefault();
        }

        public void Update(Depreciacion depreciacion)
        {
            var resul = (from p in Context.Depreciaciones
                         where p.IdDepreciacion == depreciacion.IdDepreciacion
                         select p).First();
            resul.Descripcion = depreciacion.Descripcion;
            resul.Porcentaje = depreciacion.Porcentaje;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Depreciaciones.Find(id);
            if (existe != null)
            {
                Context.Depreciaciones.Remove(existe);
                Context.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;
using System.Data;
using System.Data.Entity;

namespace Repository
{
    public class AgenciaRepository:MasterRepository,InterfaceAgencia
    {
        public List<Agencia> All()
        {
            var result = from p in Context.Agencias select p;
            return result.ToList();
        }

        public List<Agencia> ByQueryAll(string criterio)
        {
            var query = (from p in Context.Agencias select p);

            if (!string.IsNullOrEmpty(criterio))
                query = query.Where(o => o.Nombre.Contains(criterio));

            return query.ToList();
        }

        public void Store(Agencia agencia)
        {
            var addAreas = new Agencia();
            addAreas.IdAgencia = agencia.IdAgencia;
            addAreas.Nombre = agencia.Nombre;
            addAreas.CodigoAgencia = agencia.CodigoAgencia;

            foreach (var item in agencia.DetallesAgencias)
            {
                var detalle = new DetalleAgencia();
                detalle.IdAgencia = agencia.IdAgencia;
                detalle.IdArea = item.IdArea;

                addAreas.DetallesAgencias.Add(detalle);
            }

            Context.Agencias.Add(agencia);
            Context.SaveChanges();
        }

        public Agencia Find(int id)
        {
            return Context.Agencias.Find(id);
        }

        public void Update(Agencia agencia)
        {
            Context.Entry(agencia).State = EntityState.Modified;
            Context.SaveChanges(); 
        }

        public void Delete(int id)
        {
            var result = Context.Agencias.Find(id);
            if (result != null)
            {
                Context.Agencias.Remove(result);
                Context.SaveChanges();
            }     
        }
    }
}

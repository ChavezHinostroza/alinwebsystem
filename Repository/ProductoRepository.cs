﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
    public class ProductoRepository:MasterRepository,InterfaceProducto
    {
        public List<Producto> AllProducto(string criterio)
        {
            var query = from a in Context.Produsctos select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Nombre.ToUpper().Contains(criterio.ToUpper()));
            }

            return query.ToList();
        }

        public List<Producto> ByQueryNameMoreDate(string criterio, DateTime? fecha1, DateTime? fecha2)
        {
            var query = from a in Context.Produsctos select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Nombre.ToUpper().Contains(criterio.ToUpper()));
            }

            //if (idproveedor.HasValue && idproveedor != 0)
            //{
            //    query = query.Where(p => p.IdProveedorFk.Equals(idproveedor.Value));
            //}
            if (fecha1 != null && fecha2 != null)
                query = query.Where(o => o.FechaIngreso >= fecha1 && o.FechaIngreso <= fecha2);

            return query.ToList();
        }

        public void Store(Producto producto)
        {
            Context.Produsctos.Add(producto);
            Context.SaveChanges();
        }

        public Producto Find(int id)
        {
            var result = from p in Context.Produsctos
                         where p.IdProducto == id
                         select p;
            return result.FirstOrDefault();
        }

        public void Update(Producto producto)
        {
            var resul = (from p in Context.Produsctos
                         where p.IdProducto == producto.IdProducto
                         select p).First();

            resul.Nombre = producto.Nombre;
            resul.Modelo = producto.Modelo;
            resul.Marca = producto.Modelo;
            resul.Serie = producto.Serie;
            resul.Color = producto.Color;
            resul.Cantidad = producto.Cantidad;
            resul.FechaIngreso = producto.FechaIngreso;
            resul.UnidadMedida = producto.UnidadMedida;

            // resul.IdProveedorFk = producto.IdProveedorFk;


            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in Context.Produsctos where p.IdProducto == id select p).First();
            Context.Produsctos.Remove(result);
            Context.SaveChanges();
        }

        public List<Producto> AllProductoMore(string criterio, int? idproveedor)
        {
            var query = from a in Context.Produsctos select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Nombre.ToUpper().Contains(criterio.ToUpper()));
            }

            //if (idproveedor.HasValue && idproveedor != 0)
            //{
            //    query = query.Where(p => p.IdProveedorFk.Equals(idproveedor.Value));
            //}


            return query.ToList();
        }
    }
}

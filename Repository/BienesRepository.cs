﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
    public class BienesRepository : MasterRepository, InterfaceBien
    {
        public IEnumerable<Bienes> AllCriterioBienes()
        {
            var query = from p in Context.Bienes select p;
            return query;
        }

        public IEnumerable<Bienes> GetBienesBuscarFactura(string criterio, DateTime? date1, DateTime? date2)
        {
            var query = from f in Context.Bienes
                        select f;

            if (date1.HasValue && date2.HasValue)
            {
                query = from f in query
                        where f.FechaEmision >= date1.Value && f.FechaEmision <= date2.Value
                        select f;
            }

            if (!string.IsNullOrEmpty(criterio))
            {
                query = from f in query
                        where f.Descripcion.ToUpper().Contains(criterio.ToUpper())
                        select f;
            }
            return query;
        }

        public IEnumerable<Bienes> GetBienesByfechas(string criterio, DateTime? fecha1, DateTime? fecha2)
        {
            var query = from c in Context.Bienes.Include("Agencia").Include("Area").Include("Depreciacion")
                        select c;
            if (fecha1.HasValue && fecha2.HasValue)
            {
                query = from p in query
                        where p.FechaEmision >= fecha1.Value && p.FechaEmision <= fecha2.Value
                        select p;
            }

            if (!string.IsNullOrEmpty(criterio))
            {
                query = from p in query
                        where p.Area.Nombre.ToUpper().Contains(criterio.ToUpper()) || p.Descripcion.ToUpper().Contains(criterio.ToUpper())
                        || p.TipoDeProducto.ToUpper().Contains(criterio.ToUpper()) || p.NurFactura.ToUpper().Contains(criterio.ToUpper())
                        select p;
            }
            return query;
        }

        public void Store(Bienes bienes)
        {
            Context.Bienes.Add(bienes);
            Context.SaveChanges();
        }

        public Bienes Find(int id)
        {
            var result = from p in Context.Bienes.Include("Agencia").Include("Area").Include("Depreciacion")
                         where p.Idbienes == id
                         select p;
            return result.FirstOrDefault();
            //return Context.Bienes.Find(id);
        }

        public void Update(Bienes bienes)
        {
            var result = (from p in Context.Bienes
                          where p.Idbienes == bienes.Idbienes
                          select p).First();

            result.Depreciacion = bienes.Depreciacion;
            result.Grupo = bienes.Grupo;
            result.CodigoGrupo = bienes.CodigoGrupo;
            result.Estado = bienes.Estado;
            result.NurFactura = bienes.NurFactura;
            result.FechaEmision = bienes.FechaEmision;
            //result.FechaUso = bienes.FechaUso;
            result.Importe = bienes.Importe;
            result.NumbYears = bienes.NumbYears;
            result.UnidadMedida = bienes.UnidadMedida;
            result.Cantidad = bienes.Cantidad;
            result.Modelo = bienes.Modelo;
            result.Serie = bienes.Serie;
            result.Marca = bienes.Marca;
            result.Color = bienes.Color;
            result.Medidas = bienes.Medidas;
            result.NumMotor = bienes.NumMotor;
            result.Chasis = bienes.Chasis;
            result.AreaM2 = bienes.AreaM2;
            result.Dimenciones = bienes.Dimenciones;
            result.Ubicacion = bienes.Ubicacion;
            result.TipoDeProducto = bienes.TipoDeProducto;
            result.Observaciones = bienes.Observaciones;



            //result.IdProductoFk = bienes.IdProductoFk;
            result.IdAgencia = bienes.IdAgencia;
            result.IdAreafk = bienes.IdAreafk;
            result.IdDepreciacionFk = bienes.IdDepreciacionFk;

            Context.SaveChanges();
            //Context.Entry(bienes).State = EntityState.Modified;
            //Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var existe = Context.Bienes.Find(id);
            if (existe != null)
            {
                Context.Bienes.Remove(existe);
                Context.SaveChanges();
            }
        }

        public IEnumerable<Bienes> GetBienesAll(string criterio)
        {
            var query = from a in Context.Bienes.Include("Producto").Include("Agencia").Include("Area").Include("Depreciacion") select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.NurFactura.ToUpper().Contains(criterio.ToUpper()));
            }
            return query;
        }

        public IEnumerable<Bienes> ObtenerBienesDeUnTipoEspecifico(string tipoDeProducto)
        {
            var query = from b in Context.Bienes where b.TipoDeProducto == tipoDeProducto select b;

            return query.ToList();
        }

    }
}

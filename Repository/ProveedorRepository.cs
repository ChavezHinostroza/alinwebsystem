﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
    public class ProveedorRepository:MasterRepository,InterfaceProveedor
    {
        public List<Proveedor> AllCriteriosProveedor()
        {
            var result = from p in Context.Proveedores select p;
            return result.ToList();
        }

        public void Store(Proveedor proveedor)
        {
            Context.Proveedores.Add(proveedor);
            Context.SaveChanges();
        }

        public Proveedor Find(int id)
        {
            var result = from p in Context.Proveedores
                         where p.IdProveedor == id
                         select p;
            return result.FirstOrDefault();
        }

        public void Update(Proveedor proveedor)
        {
            var result = (from p in Context.Proveedores
                          where p.IdProveedor == proveedor.IdProveedor
                          select p).First();

            result.Representante = proveedor.Representante;
            result.Correo = proveedor.Correo;
            result.Telefono = proveedor.Telefono;
            result.RazonSocial = proveedor.RazonSocial;
            result.RUC = proveedor.RUC;
            result.TelefonoOp = proveedor.TelefonoOp;
            result.Direccion = proveedor.Direccion;

            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in Context.Proveedores where p.IdProveedor == id select p).First();
            Context.Proveedores.Remove(result);
            Context.SaveChanges();
        }

        public List<Proveedor> ByQueryAll(string criterio)
        {
            var query = from a in Context.Proveedores select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.RazonSocial.ToUpper().Contains(criterio.ToUpper()));
            }
            return query.ToList();
        }

        public int BuscaEmailRepetido(string email, int id)
        {
            return (from p in Context.Proveedores where p.Correo == email && p.IdProveedor != id select p).Count();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
    public class DetalleAgenciaRepository:MasterRepository,InterfaceDetalleAgencia
    {
        public IEnumerable<DetalleAgencia> GetDetalleByNro(string nroAgencia)
        {
            var query = from c in Context.DetalleAgencia.Include("Agencia").Include("Area")
                        select c;
            if (!string.IsNullOrEmpty(nroAgencia))
            {
                query = from p in query
                        where p.Agencia.Nombre == nroAgencia
                        select p;
            }
            return query;
        }

        public void AddDetalle(DetalleAgencia detalle)
        {
            //Context.DetallesCompra.Add(detalle);
            Context.Database.ExecuteSqlCommand("AgregarStock @Nombre,@IdArea",
            new SqlParameter("@Nombre", detalle.IdAgencia),
            new SqlParameter("@IdArea", detalle.IdArea));

            Context.SaveChanges();
        }

        public void UpdateDetalle(DetalleAgencia detalle)
        {
            throw new NotImplementedException();
        }

        public void RemoveDetalle(string nroAgencia)
        {
            var existe = Context.DetalleAgencia.Find(nroAgencia);

            if (existe != null)
            {
                Context.DetalleAgencia.Remove(existe);
                Context.SaveChanges();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataBase;
namespace Repository
{
 public abstract class MasterRepository
    {
    
         private readonly RondesaContext _context;

         protected MasterRepository()
         {
             if (_context == null)
                 _context = new RondesaContext();
         }

         protected RondesaContext Context
         {
             get { return _context; }
         }
     }
}


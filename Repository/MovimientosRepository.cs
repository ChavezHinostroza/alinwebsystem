﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
    public class MovimientosRepository:MasterRepository,InterfaceMovimientos
    {
        public Producto ObtenerProductoPorNombre(string NameProduc)
        {
            var query = from p in Context.Produsctos where p.Nombre == NameProduc select p;
            return query.SingleOrDefault();
        }

        public List<Producto> AllProducto()
        {
            var query = from a in Context.Produsctos select a;
            return query.ToList();
        }

        public List<Movimientos> ByQueryMoreDate(string criterio, DateTime? fecha1, DateTime? fecha2)
        {
            var query = from a in Context.Movimientos.Include("Producto").Include("Area").Include("Agencia").Include("Proveedor") select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Producto.Nombre.ToUpper().Contains(criterio.ToUpper()));
            }


            if (fecha1 != null && fecha2 != null)
                query = query.Where(o => o.Fecha >= fecha1 && o.Fecha <= fecha2);

            return query.ToList();
        }

        public void Store(Movimientos movimientos)
        {
            Context.Movimientos.Add(movimientos);
            Context.SaveChanges();
        }

        public Producto obtenerProoductoById(int id)
        {
            var query = from a in Context.Produsctos where a.IdProducto == id select a;
            return query.FirstOrDefault();
        }
    }
}

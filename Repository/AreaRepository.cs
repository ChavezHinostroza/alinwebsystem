﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Interfaces;
using Entities;
using DataBase;

namespace Repository
{
  public  class AreaRepository:MasterRepository,InterfaceArea
    {
        public List<Area> All()
        {
            var query = from a in Context.Areas
                        select a;
            return query.ToList();
        }

        public List<Area> AllArea(string criterio)
        {
            var query = from a in Context.Areas select a;
            if (!string.IsNullOrEmpty(criterio))
            {
                query = query.Where(a => a.Nombre.ToUpper().Contains(criterio.ToUpper()));
            }
            return query.ToList();
        }

        public void Store(Area area)
        {
            Context.Areas.Add(area);
            Context.SaveChanges();
        }

        public Area Find(int id)
        {
            var result = from p in Context.Areas
                         where p.IdArea == id
                         select p;
            return result.FirstOrDefault();
        }

        public void Update(Area Area)
        {
            var resul = (from p in Context.Areas
                         where p.IdArea == Area.IdArea
                         select p).First();
            resul.Nombre = Area.Nombre;
            resul.CodigoArea = Area.CodigoArea;
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            var result = (from p in Context.Areas
                          where p.IdArea == id
                          select p).First();
            Context.Areas.Remove(result);
            Context.SaveChanges();
        }
    }
}

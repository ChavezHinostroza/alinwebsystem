﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataBase.Mapping;
using System.Data.Entity;
using Entities;

namespace DataBase
{
  public class RondesaContext:DbContext
  {
        //public RondesaContext()
        //{
        //    Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RondesaContext>());
        //}
        public DbSet<Producto> Produsctos { get; set; }
        public DbSet<Proveedor> Proveedores { get; set; }
        public DbSet<Bienes> Bienes { get; set; }
        public DbSet<Agencia> Agencias { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Depreciacion> Depreciaciones { get; set; }
        public DbSet<Movimientos> Movimientos { get; set; }
        public DbSet<DetalleAgencia> DetalleAgencia { get; set; }
        public DbSet<Factura> Facturas { get; set; }
      

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductoMap());
            modelBuilder.Configurations.Add(new ProveedorMap());
            modelBuilder.Configurations.Add(new BienesMap());
            modelBuilder.Configurations.Add(new AgenciaMap());
            modelBuilder.Configurations.Add(new AreaMap());
            modelBuilder.Configurations.Add(new DepreciacionMap());
            modelBuilder.Configurations.Add(new MovimientosMap());
            modelBuilder.Configurations.Add(new DetalleAgenciaMap());
            modelBuilder.Configurations.Add(new FacturaMap());
            
         
        }
    }
}

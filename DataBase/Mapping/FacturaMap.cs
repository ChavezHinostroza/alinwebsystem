﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class FacturaMap:EntityTypeConfiguration<Factura>
    {
        public FacturaMap()
        {
            this.HasKey(p => p.IdFactura);
            this.Property(p => p.IdFactura).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(p => p.NumDocumento).IsOptional();
            this.Property(p => p.FechaEmision).IsOptional();
            this.Property(p => p.GuiaRemision).IsOptional();
            this.Property(p => p.NumGuiasRemision).IsOptional();

            this.ToTable("Factura");
        }

    }
}

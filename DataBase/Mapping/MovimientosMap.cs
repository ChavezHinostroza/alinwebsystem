﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
    public class MovimientosMap:EntityTypeConfiguration<Movimientos>
    {
        public MovimientosMap()
        {
            this.HasKey(p => p.IdMovimientos);
            this.Property(p => p.IdMovimientos).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Fecha).IsRequired();
            this.Property(p => p.Tipo).IsRequired();
            this.Property(p => p.NroMovimiento).IsRequired();
            this.Property(p => p.NumFactura).IsRequired();
            this.Property(p => p.Stock).IsRequired();
            this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.IdProductoFk).WillCascadeOnDelete(true);
            this.HasRequired(p => p.Agencia).WithMany().HasForeignKey(p => p.IdAgencia).WillCascadeOnDelete(true);
            this.HasRequired(p => p.Area).WithMany().HasForeignKey(p => p.IdAreafk).WillCascadeOnDelete(true);
            this.HasRequired(p => p.Proveedor).WithMany().HasForeignKey(p => p.IdProveedorFk).WillCascadeOnDelete(true);
            this.ToTable("Movimientos");
        }
    }
}

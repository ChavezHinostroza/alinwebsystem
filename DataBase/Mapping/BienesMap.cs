﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
   public class BienesMap:EntityTypeConfiguration<Bienes>
    {
        public BienesMap()
        {
            this.HasKey(p => p.Idbienes);
            this.Property(p => p.Idbienes).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Descripcion).HasMaxLength(200).IsOptional();
            this.Property(p => p.Grupo).IsRequired();
            this.Property(p => p.CodigoGrupo).IsOptional();
            this.Property(p => p.Estado).HasMaxLength(50).IsRequired();
            this.Property(p => p.NurFactura).IsRequired();
            this.Property(p => p.FechaEmision).IsRequired();
            //this.Property(p => p.FechaUso).IsOptional();
            this.Property(p => p.Importe).IsRequired();
            this.Property(p => p.NumbYears).IsOptional();
            this.Property(p => p.UnidadMedida).HasMaxLength(100).IsRequired();
            this.Property(p => p.Cantidad).IsRequired();
            this.Property(p => p.Modelo).IsOptional();
            this.Property(p => p.Serie).IsOptional();
            this.Property(p => p.Marca).IsOptional();
            this.Property(p => p.Color).IsOptional();
            this.Property(p => p.Medidas).IsOptional();
            this.Property(p => p.NumMotor).IsOptional();
            this.Property(p => p.Chasis).IsOptional();
            this.Property(p => p.AreaM2).IsOptional();
            this.Property(p => p.Dimenciones).IsOptional();
            this.Property(p => p.Ubicacion).IsOptional();
            this.Property(p => p.TipoDeProducto).IsOptional();
            this.Property(p => p.Observaciones).IsOptional();

            this.Property(p => p.DepreciacionMes).HasPrecision(9, 2).IsOptional();
            this.Property(p => p.DepreciacionAcumulada).HasPrecision(9, 2).IsOptional();
            this.Property(p => p.ValorNeto).HasPrecision(9, 2).IsOptional();
            this.Property(p => p.NumAnios).IsOptional();
            this.Property(p => p.AlDia).HasPrecision(9,2).IsOptional();

            //this.Property(p => p.PrecioInicial).IsOptional();
            //this.Property(p => p.MontoActual).IsOptional();


            //this.HasRequired(p => p.Producto).WithMany().HasForeignKey(p => p.IdProductoFk).WillCascadeOnDelete(true);
            this.HasRequired(p => p.Agencia).WithMany().HasForeignKey(p => p.IdAgencia).WillCascadeOnDelete(true);
            this.HasRequired(p => p.Area).WithMany().HasForeignKey(p => p.IdAreafk).WillCascadeOnDelete(true);
            this.HasRequired(p => p.Depreciacion).WithMany().HasForeignKey(p => p.IdDepreciacionFk).WillCascadeOnDelete(true);
            this.ToTable("Bienes");
        }
    }
}

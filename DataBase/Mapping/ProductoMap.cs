﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;
using Entities;

namespace DataBase.Mapping
{
    public class ProductoMap:EntityTypeConfiguration<Producto>
    {
        public ProductoMap()
       {
           this.HasKey(p => p.IdProducto);
           this.Property(p => p.IdProducto).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           this.Property(p => p.Nombre).IsRequired().HasMaxLength(100);
           this.Property(p => p.Modelo).IsOptional().HasMaxLength(50);
           this.Property(p => p.Serie).IsOptional();
           this.Property(p => p.Marca).IsOptional().HasMaxLength(50);
           this.Property(p => p.Color).IsOptional().HasMaxLength(50);
           this.Property(p => p.FechaIngreso).HasColumnType("datetime2").IsOptional();
           this.Property(p => p.Cantidad).IsOptional();
           this.Property(p => p.UnidadMedida).IsRequired();
           this.Property(p => p.TipoDeProducto).IsRequired();
           this.Property(p => p.Talla).IsOptional();
           this.Property(p => p.Sexo).IsOptional();
           this.Property(p => p.Precio).IsRequired();
           
           this.Property(p => p.Observaciones).IsOptional();
           this.Property(p => p.FechaVencimiento).HasColumnType("datetime2").IsOptional();
           


                     

           this.ToTable("Producto");
       }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;

namespace Validator.ProveedorValidators
{
   public class ProveedorValidators
    {
       public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Proveedor proveedor)
        {
            Pass(proveedor);
        }

        public virtual bool Pass(Proveedor proveedor)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(proveedor.RUC))
                errors.Add("RUC", "Ingrese RUC");
            else
            {
                if (!regNumeros.IsMatch(proveedor.RUC))
                    errors.Add("RUC", "El RUC debe contener solo dígitos");
                else
                {
                    if (proveedor.RUC.Length != 11)
                        errors.Add("RUC", "El RUC debe contar con 11 dígitos");
                }
            }

            if (String.IsNullOrEmpty(proveedor.Representante))
                errors.Add("Representante", "Ingrese Nombre ");
            else
            {
                if (!regLetras.IsMatch(proveedor.Representante))
                    errors.Add("Representante", "El Nombre debe contener solo Letras");
                else
                {
                    if (proveedor.Representante.Length >= 30)
                        errors.Add("Representante", "El Nombre debe tener hasta 30 letras");
                }
            }

           if (String.IsNullOrEmpty(proveedor.Direccion))
                errors.Add("Direccion", "Ingrese Dirección");
            else
            {
                if (proveedor.Direccion.Length >= 30)
                    errors.Add("Direccion", "La Direccion debe tener hasta 30 caracteres");
            }

           if (String.IsNullOrEmpty(proveedor.RazonSocial))
               errors.Add("RazonSocial", "Ingrese Rázon Social");
           else
           {
               if (proveedor.Direccion.Length >= 30)
                   errors.Add("Direccion", "La Direccion debe tener hasta 30 caracteres");
           }


            if (String.IsNullOrEmpty(proveedor.Telefono))
                errors.Add("Telefono", "Ingrese Teléfono");
            else
            {
                if (!regNumeros.IsMatch(proveedor.Telefono))
                    errors.Add("Telefono", "El Teléfono debe contener solo dígitos");
                else
                {
                    if (proveedor.Telefono.Length != 9)
                        errors.Add("Telefono", "El Telefono debe contar con 9 digitos");
                }
            }

            if (String.IsNullOrEmpty(proveedor.Correo))
                errors.Add("Correo", "Ingrese Email");
            else
            {
                if (!regEmail.IsMatch(proveedor.Correo))
                    errors.Add("Email", "Email no válido, ejemplo: aabbcc@ejemplo.com");
                else
                {
                    if (proveedor.Correo.Length >= 30)
                        errors.Add("Email", "El Email debe tener hasta 30 Caracteres");
                    //else
                    //{
                    //    if (!BuscaEmailRepetido(proveedor.Correo, proveedor.IdProveedor))
                    //        errors.Add("Email", "El Email Ya existe");
                    //}
                }
            }

            return true;
    }
        //private bool BuscaEmailRepetido(string email, int id)
        //{
          
        //    var repo = new ProveedorRepository(new RondesaContext());
        //    if (repo.BuscaEmailRepetido(email, id) > 0)
        //        return false;
        //    return true;
        //}

    }
}

//
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;

namespace Validator.AgenciaValidator
{
   public class AgenciaValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Agencia agencia)
        {
            Pass(agencia);
        }

        public virtual bool Pass(Agencia agencia)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");


            if (String.IsNullOrEmpty(agencia.Nombre))
                errors.Add("Nombre", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(agencia.Nombre))
                    errors.Add("Nombre", "El Nombre debe contener solo letras");
            }

            if (String.IsNullOrEmpty(agencia.CodigoAgencia))
                errors.Add("CodigoAgencia", "Ingrese código de área");
            else
            {
                if (!regLetras.IsMatch(agencia.CodigoAgencia))
                    errors.Add("CodigoAgencia", "El código de área debe contener solo letras");
                else
                {
                    if (agencia.CodigoAgencia.Length != 2)
                        errors.Add("CodigoAgencia", "El código de la agencia debe contar con 2 letras");
                }
            }
            return true;
        }
    }
}

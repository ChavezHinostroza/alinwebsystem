﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;


namespace Validator.DepreciacionValidator
{
  public  class DepreciacionValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Depreciacion depreciacion)
        {
            Pass(depreciacion);
        }

        public virtual bool Pass(Depreciacion depreciacion)
        {
            Regex regNumeros = new Regex(@"[0-9]\d{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");


            if (String.IsNullOrEmpty(depreciacion.Descripcion))
                errors.Add("Descripcion", "Ingrese Descripción");
            else
            {
                if (!regLetras.IsMatch(depreciacion.Descripcion))
                    errors.Add("Descripcion", "La descripción debe contener solo letras");
            }

            if (depreciacion.Porcentaje == null || depreciacion.Porcentaje == 0)
            {
                errors.Add("Porcentaje", "Ingrese Porcentaje");
            }
            //if (Int32.IsNullOrEmpty(depreciacion.Porcentaje))
            //    errors.Add("Porcentaje", "Ingrese Porcentaje");
            //else
            //{
            //    if (!regNumeros.IsMatch(depreciacion.Porcentaje))
            //        errors.Add("Porcentaje", "Ingrese solo números");
            //}
            return true;
        }
    }
}

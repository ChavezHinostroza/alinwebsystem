﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;

namespace Validator.MovimientosValidators
{
   public class MovimientosValidators
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Movimientos movimientos)
        {
            Pass(movimientos);
        }

        public virtual bool Pass(Movimientos movimientos)
        {
            Regex regNumeros = new Regex(@"[0-9]\d{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");


            if (String.IsNullOrEmpty(movimientos.Fecha.ToString()))
                errors.Add("Fecha", "Seleccione Fecha");


            if (movimientos.Stock == null)
            {
                errors.Add("Stock", "Stock es obligatorio");
            }
            else
            {
                if (movimientos.Stock < 0)
                {
                    errors.Add("Stock", "Stock no puede ser negativo");
                }
            }

            if (movimientos.NumFactura == null)
            {
                errors.Add("NumFactura", "NumFactura es obligatorio");
            }
            else
            {
                if (movimientos.NumFactura < 0)
                {
                    errors.Add("NumFactura", "# Factura no puede ser negativo");
                }
            }

            return true;
        }
    }
}

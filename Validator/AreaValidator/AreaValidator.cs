﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;

namespace Validator.AreaValidator
{
   public class AreaValidator
    {
       public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Area area)
        {
            Pass(area);
        }

        public virtual bool Pass(Area area)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");
            //Regex regLetras = new Regex("^[A-Za-z ]+$");
            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
           

            if (String.IsNullOrEmpty(area.Nombre))
                errors.Add("Nombre", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(area.Nombre))
                    errors.Add("Nombre", "El Nombre debe contener solo letras");
             }

            if (String.IsNullOrEmpty(area.CodigoArea))
                errors.Add("CodigoArea", "Ingrese código de área");
            else
            {
                if (!regLetras.IsMatch(area.CodigoArea))
                    errors.Add("CodigoArea", "El código de área debe contener solo letras");
                else
                {
                    if (area.CodigoArea.Length != 2)
                        errors.Add("CodigoArea", "El código de área debe contar con 2 letras");
                }
            }
            return true;
        }
       
    }
}

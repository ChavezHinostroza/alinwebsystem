﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;

namespace Validator.ProductoValidators
{
   public class ProductoValidators
    {

       
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Producto producto)
        {
            if (producto.TipoDeProducto == "Otros")
            {
                PassOtros(producto);
            }
            if (producto.TipoDeProducto == "Suministros")
            {
                PassSumministros(producto);
            }
            if (producto.TipoDeProducto == "Vestuario")
            {
                PassVestuarios(producto);
            }
            
        }

        public virtual bool PassOtros(Producto producto)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(producto.Nombre))
                errors.Add("Nombre", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(producto.Nombre))
                    errors.Add("Nombre", "El nombre debe contener solo letras");
            }



            if (String.IsNullOrEmpty(producto.Modelo))
                errors.Add("Modelo", "Ingrese Modelo ");
            else
            {
                if (!regLetras.IsMatch(producto.Modelo))
                    errors.Add("Modelo", "El Modelo debe contener solo Letras");

            }

            if (String.IsNullOrEmpty(producto.Marca))
                errors.Add("Marca", "Ingrese Marca ");
            else
            {
                if (!regLetras.IsMatch(producto.Marca))
                    errors.Add("Marca", "El Marca debe contener solo Letras");

            }

            if (String.IsNullOrEmpty(producto.Serie))
                errors.Add("Serie", "Ingrese Serie ");



            //vestuario

            //if (String.IsNullOrEmpty(producto.Color))
            //    errors.Add("Color", "Ingrese Color");
            //else
            //{
            //    if (!regLetras.IsMatch(producto.Nombre))
            //        errors.Add("Color", "El Color debe contener solo letras");
            //}


            return true;
        }

        public virtual bool PassSumministros(Producto producto)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(producto.Nombre))
                errors.Add("Nombre", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(producto.Nombre))
                    errors.Add("Nombre", "El nombre debe contener solo letras");
            }

            if (String.IsNullOrEmpty(producto.Modelo))
                errors.Add("Modelo", "Ingrese Modelo ");
            else
            {
                if (!regLetras.IsMatch(producto.Modelo))
                    errors.Add("Modelo", "El Modelo debe contener solo Letras");

            }

            if (String.IsNullOrEmpty(producto.Marca))
                errors.Add("Marca", "Ingrese Marca ");
            else
            {
                if (!regLetras.IsMatch(producto.Marca))
                    errors.Add("Marca", "El Marca debe contener solo Letras");

            }

            if (String.IsNullOrEmpty(producto.Serie))
                errors.Add("Serie", "Ingrese Serie ");


            if (producto.Precio == null)
            {
                errors.Add("Precio", "Precio es obligatorio");
            }
            else 
            {
                if (producto.Precio < 0)
                {
                    errors.Add("Precio", "Precio no puede ser negativo");
                }
            }
               
            return true;
        }

        public virtual bool PassVestuarios(Producto producto)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");

            if (String.IsNullOrEmpty(producto.Nombre))
                errors.Add("Nombre", "Ingrese Nombre");
            else
            {
                if (!regLetras.IsMatch(producto.Nombre))
                    errors.Add("Nombre", "El nombre debe contener solo letras");
            }



            if (String.IsNullOrEmpty(producto.Color))
                errors.Add("Color", "Ingrese Color ");
            else
            {
                if (!regLetras.IsMatch(producto.Color))
                    errors.Add("Color", "El Color debe contener solo Letras");

            }

            if (producto.Precio == null)
            {
                errors.Add("Precio", "Precio es obligatorio");
            }
            else
            {
                if (producto.Precio < 0)
                {
                    errors.Add("Precio", "Precio no puede ser negativo");
                }
            }


            if (producto.Cantidad == null)
            {
                errors.Add("Cantidad", "Cantidad es obligatorio");
            }
            else
            {
                if (producto.Cantidad < 0)
                {
                    errors.Add("Cantidad", "Cantidad no puede ser negativo");
                }
            }

            


            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using Entities;
using Interfaces;
using Repository;
using DataBase;

namespace Validator.BienesValidator
{
   public class BienesValidator
    {
        public Dictionary<String, String> errors = new Dictionary<string, string>();
        public void Execute(Bienes bienes)
        {
            if (bienes.TipoDeProducto == "Muebles")
            {
                PassMuebles(bienes);
            }
            if (bienes.TipoDeProducto == "Computo")
            {
                PassComputo(bienes);
            }
            if (bienes.TipoDeProducto == "Vehiculos")
            {
                PassVehiculos(bienes);
            }
            if (bienes.TipoDeProducto == "Otros")
            {
                PassOtros(bienes);
            }

        }

        public virtual bool PassMuebles(Bienes bienes)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");


            if (String.IsNullOrEmpty(bienes.Descripcion))
                errors.Add("Descripcion", "Ingrese Descripcion");
            else
            {
                if (!regLetras.IsMatch(bienes.Descripcion))
                    errors.Add("Descripcion", "La descripción debe contener solo letras");
            }



            if (String.IsNullOrEmpty(bienes.Color))
                errors.Add("Color", "Ingrese Color ");
            else
            {
                if (!regLetras.IsMatch(bienes.Color))
                    errors.Add("Color", "El Color debe contener solo Letras");

            }


            if (bienes.Cantidad == null)
            {
                errors.Add("Cantidad", "Cantidad es obligatorio");
            }
            else
            {
                if (bienes.Cantidad < 0)
                {
                    errors.Add("Cantidad", "Cantidad no puede ser negativo");
                }
            }


            if (String.IsNullOrEmpty(bienes.Medidas))
                errors.Add("Medidas", "Ingrese Medidas ");

            if (bienes.Importe == null)
            {
                errors.Add("Importe", "Importe es obligatorio");
            }
            else
            {
                if (bienes.Importe < 0)
                {
                    errors.Add("Importe", "Importe no puede ser negativo");
                }
            }

            if (String.IsNullOrEmpty(bienes.NurFactura))
                errors.Add("NurFactura", "Ingrese # Factura");
            else
            {
                if (!regNumeros.IsMatch(bienes.NurFactura))
                    errors.Add("NurFactura", "El # Factura debe contener solo números");
            }

            return true;
        }

        public virtual bool PassComputo(Bienes bienes)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");


            if (String.IsNullOrEmpty(bienes.Descripcion))
                errors.Add("Descripcion", "Ingrese Descripcion");
            else
            {
                if (!regLetras.IsMatch(bienes.Descripcion))
                    errors.Add("Descripcion", "La descripción debe contener solo letras");
            }

            if (String.IsNullOrEmpty(bienes.Marca))
                errors.Add("Marca", "Ingrese Marca");
            else
            {
                if (!regLetras.IsMatch(bienes.Marca))
                    errors.Add("Marca", "La Marca debe contener solo letras");
            }


            if (String.IsNullOrEmpty(bienes.Modelo))
                errors.Add("Modelo", "Ingrese Modelo");
            else
            {
                if (!regLetras.IsMatch(bienes.Modelo))
                    errors.Add("Modelo", "La Modelo debe contener solo letras");
            }


            if (String.IsNullOrEmpty(bienes.Color))
                errors.Add("Color", "Ingrese Color ");
            else
            {
                if (!regLetras.IsMatch(bienes.Color))
                    errors.Add("Color", "El Color debe contener solo Letras");

            }

            if (String.IsNullOrEmpty(bienes.Serie))
                errors.Add("Serie", "Ingrese Serie ");
            


            if (bienes.Cantidad == null)
            {
                errors.Add("Cantidad", "Cantidad es obligatorio");
            }
            else
            {
                if (bienes.Cantidad < 0)
                {
                    errors.Add("Cantidad", "Cantidad no puede ser negativo");
                }
            }


            

            if (bienes.Importe == null)
            {
                errors.Add("Importe", "Importe es obligatorio");
            }
            else
            {
                if (bienes.Importe < 0)
                {
                    errors.Add("Importe", "Importe no puede ser negativo");
                }
            }

            if (String.IsNullOrEmpty(bienes.NurFactura))
                errors.Add("NurFactura", "Ingrese # Factura");
            else
            {
                if (!regNumeros.IsMatch(bienes.NurFactura))
                    errors.Add("NurFactura", "El # Factura debe contener solo números");
            }

            return true;
        }

        public virtual bool PassOtros(Bienes bienes)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");


            if (String.IsNullOrEmpty(bienes.Descripcion))
                errors.Add("Descripcion", "Ingrese Descripcion");
            else
            {
                if (!regLetras.IsMatch(bienes.Descripcion))
                    errors.Add("Descripcion", "La descripción debe contener solo letras");
            }

            if (String.IsNullOrEmpty(bienes.Marca))
                errors.Add("Marca", "Ingrese Marca");
            else
            {
                if (!regLetras.IsMatch(bienes.Marca))
                    errors.Add("Marca", "La Marca debe contener solo letras");
            }


            if (String.IsNullOrEmpty(bienes.Modelo))
                errors.Add("Modelo", "Ingrese Modelo");
            else
            {
                if (!regLetras.IsMatch(bienes.Modelo))
                    errors.Add("Modelo", "La Modelo debe contener solo letras");
            }


            if (String.IsNullOrEmpty(bienes.Color))
                errors.Add("Color", "Ingrese Color ");
            else
            {
                if (!regLetras.IsMatch(bienes.Color))
                    errors.Add("Color", "El Color debe contener solo Letras");

            }

            if (String.IsNullOrEmpty(bienes.Serie))
                errors.Add("Serie", "Ingrese Serie ");



            if (bienes.Cantidad == null)
            {
                errors.Add("Cantidad", "Cantidad es obligatorio");
            }
            else
            {
                if (bienes.Cantidad < 0)
                {
                    errors.Add("Cantidad", "Cantidad no puede ser negativo");
                }
            }




            if (bienes.Importe == null)
            {
                errors.Add("Importe", "Importe es obligatorio");
            }
            else
            {
                if (bienes.Importe < 0)
                {
                    errors.Add("Importe", "Importe no puede ser negativo");
                }
            }

            if (String.IsNullOrEmpty(bienes.NurFactura))
                errors.Add("NurFactura", "Ingrese # Factura");
            else
            {
                if (!regNumeros.IsMatch(bienes.NurFactura))
                    errors.Add("NurFactura", "El # Factura debe contener solo números");
            }

            return true;
        }


        public virtual bool PassVehiculos(Bienes bienes)
        {
            Regex regNumeros = new Regex(@"[0-9]{1,9}(\.[0-9]{0,2})?$");

            Regex regLetras = new Regex(@"[a-zA-ZñÑ\s]");
            Regex regEmail = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");


            if (String.IsNullOrEmpty(bienes.Descripcion))
                errors.Add("Descripcion", "Ingrese Descripcion");
            else
            {
                if (!regLetras.IsMatch(bienes.Descripcion))
                    errors.Add("Descripcion", "La descripción debe contener solo letras");
            }

            if (String.IsNullOrEmpty(bienes.Marca))
                errors.Add("Marca", "Ingrese Marca");
            else
            {
                if (!regLetras.IsMatch(bienes.Marca))
                    errors.Add("Marca", "La Marca debe contener solo letras");
            }


            if (String.IsNullOrEmpty(bienes.Chasis))
                errors.Add("Chasis", "Ingrese Chasis");
            


            if (String.IsNullOrEmpty(bienes.Color))
                errors.Add("Color", "Ingrese Color ");
            else
            {
                if (!regLetras.IsMatch(bienes.Color))
                    errors.Add("Color", "El Color debe contener solo Letras");

            }


            if (bienes.Importe == null)
            {
                errors.Add("Importe", "Importe es obligatorio");
            }
            else
            {
                if (bienes.Importe < 0)
                {
                    errors.Add("Importe", "Importe no puede ser negativo");
                }
            }

            if (String.IsNullOrEmpty(bienes.NurFactura))
                errors.Add("NurFactura", "Ingrese # Factura");
            else
            {
                if (!regNumeros.IsMatch(bienes.NurFactura))
                    errors.Add("NurFactura", "El # Factura debe contener solo números");
            }

            if (String.IsNullOrEmpty(bienes.NumMotor))
                errors.Add("NumMotor", "Ingrese # Motor");
            

            return true;
        }
    }
}

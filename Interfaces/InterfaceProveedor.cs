﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace Interfaces
{
  public  interface InterfaceProveedor
    {
        List<Proveedor> AllCriteriosProveedor();
        void Store(Proveedor proveedor);
        Proveedor Find(int id);
        void Update(Proveedor proveedor);
        void Delete(int id);
        List<Proveedor> ByQueryAll(string query);

       int BuscaEmailRepetido(string email, int id);
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace Interfaces
{
    public interface InterfaceDetalleAgencia
    {
        IEnumerable<DetalleAgencia> GetDetalleByNro(string nroAgencia);
        void AddDetalle(DetalleAgencia detalle);
        void UpdateDetalle(DetalleAgencia detalle);
        void RemoveDetalle(string nroAgencia);
    }
}

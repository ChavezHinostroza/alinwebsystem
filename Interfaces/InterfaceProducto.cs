﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace Interfaces
{
    public interface InterfaceProducto
    {
        List<Producto> AllProducto(string criterio);
        List<Producto> ByQueryNameMoreDate(string criterio, DateTime? fecha1, DateTime? fecha2);
        void Store(Producto producto);
        Producto Find(int id);
        void Update(Producto producto);
        void Delete(int id);
    }
}

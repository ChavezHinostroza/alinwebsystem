﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace Interfaces
{
    public interface InterfaceDepreciacion
    {
        IEnumerable<Depreciacion> AllDepreciacion(string descripcion);
        void Store(Depreciacion depreciacion);
        Depreciacion Find(int id);
        void Update(Depreciacion depreciacion);
        void Delete(int id);
    }
}

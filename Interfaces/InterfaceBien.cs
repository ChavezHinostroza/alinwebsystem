﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace Interfaces
{
    public interface InterfaceBien
    {
        IEnumerable<Bienes> AllCriterioBienes();
        IEnumerable<Bienes> GetBienesBuscarFactura(string criterio, DateTime? date1, DateTime? date2);
        IEnumerable<Bienes> GetBienesByfechas(string criterio, DateTime? fecha1, DateTime? fecha2);
        void Store(Bienes bienes);
        Bienes Find(int id);
        void Update(Bienes bienes);
        void Delete(int id);

        IEnumerable<Bienes> ObtenerBienesDeUnTipoEspecifico(string tipoDeProducto);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
namespace Interfaces
{
    public interface InterfaceMovimientos
    {
        Producto ObtenerProductoPorNombre(string NameProduc);
        List<Producto> AllProducto();
        List<Movimientos> ByQueryMoreDate(string criterio, DateTime? fecha1, DateTime? fecha2);
        void Store(Movimientos movimientos);
        Producto obtenerProoductoById(int id);
    }
}

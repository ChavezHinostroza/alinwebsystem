﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Interfaces
{
    public interface InterfaceAgencia
    {
        List<Agencia> All();
        List<Agencia> ByQueryAll(string criterio);
        void Store(Agencia agencia);
        Agencia Find(int id);
        void Update(Agencia agencia);
        void Delete(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Entities;
using DataBase;

namespace GenerateDb
{
    class Program
    {
        static void Main(string[] args)
        {
            var area01 = new Area()
            {
                Nombre = "Sistemas",
                CodigoArea = "SI"
            };

            var _context = new RondesaContext();
            Console.WriteLine("Creando Base De Datos");
            _context.Areas.Add(area01);
            _context.SaveChanges();

            Console.WriteLine("Base de datos creada con exito!!");
            Console.ReadLine();
        }
    }
}

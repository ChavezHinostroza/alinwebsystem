﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class DetalleAgencia
    {
        public Int32 IdArea { get; set; }
        public Area Area { get; set; }

        public Int32 IdAgencia { get; set; }
        public Agencia Agencia { get; set; }
    }
}

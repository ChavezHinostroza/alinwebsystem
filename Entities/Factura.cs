﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Factura
    {
        public Int32 IdFactura { get; set; }
        public string TipoDocumento { get; set; }
        public string NumDocumento { get; set; }
        public DateTime FechaEmision { get; set; }
        public string GuiaRemision { get; set; }
        public String NumGuiasRemision { get; set; }
    }
}

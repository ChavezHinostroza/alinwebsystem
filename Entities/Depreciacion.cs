﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
 public  class Depreciacion
    {
        public Int32 IdDepreciacion { get; set; }
        public string Descripcion { get; set; }
        public Int32 Porcentaje { get; set; }
    }
}

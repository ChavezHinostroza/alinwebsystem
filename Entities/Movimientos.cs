﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Movimientos
    {
        public Int32 IdMovimientos { get; set; }
        public DateTime Fecha { get; set; }
        public string Tipo { get; set; }
        public int NroMovimiento { get; set; }

        public Int32 IdProductoFk { get; set; }
        public Producto Producto { get; set; }

        public Int32 IdAgencia { get; set; }
        public Agencia Agencia { get; set; }
        public Int32 NumFactura { get; set; }

        public Int32 IdAreafk { get; set; }
        public Area Area { get; set; }

        public Int32 IdProveedorFk { get; set; }
        public Proveedor Proveedor { get; set; }

        public Int32 Stock { get; set; }
    }
}

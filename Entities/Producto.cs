﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Producto
    {
        public Int32 IdProducto { get; set; }
        public string Nombre { get; set; }
        public string Modelo { get; set; }
        public string Serie { get; set; }
        public string Marca { get; set; }
        //public Int32 Stock { get; set; }
        public string Color { get; set; }

        public Int32 Cantidad { get; set; }
        public string UnidadMedida { get; set; }
        public string TipoDeProducto { get; set; }
        public string Talla { get; set; }
        public string Sexo { get; set; }
        public decimal Precio { get; set; }
        public string Observaciones { get; set; }

        public DateTime FechaIngreso { get; set; }
        public DateTime FechaVencimiento { get; set; }
    }
}

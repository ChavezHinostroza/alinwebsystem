﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
   public class Bienes
    {
        public Int32 Idbienes { get; set; }
        public string Descripcion { get; set; }
        public Int32 Grupo { get; set; }
        public string CodigoGrupo { get; set; }
        public string Estado { get; set; }
        public string NurFactura { get; set; }
        public DateTime FechaEmision { get; set; }
        //public DateTime FechaUso { get; set; }
        public decimal Importe { get; set; }
        public Int32 Cantidad { get; set; }
        public string UnidadMedida { get; set; }
        public Int32 NumbYears { get; set; }
        //Atributos propios de un bien
        public string Modelo { get; set; }
        public string Serie { get; set; }
        public string Marca { get; set; }
        public string Color { get; set; }
        public string Medidas { get; set; }
        public string NumMotor { get; set; }
        public string Chasis { get; set; }
        public string AreaM2 { get; set; }
        public string Dimenciones { get; set; }
        public string Ubicacion { get; set; }

        public string TipoDeProducto { get; set; }
        public string Observaciones { get; set; }

        public Int32 NumAnios { get; set; }

        public decimal DepreciacionMes { get; set; }
        public decimal DepreciacionAcumulada { get; set; }
        public decimal ValorNeto { get; set; }
        public decimal AlDia { get; set; }

        
        public Int32 IdAgencia { get; set; }
        public Agencia Agencia { get; set; }

        public Int32 IdAreafk { get; set; }
        public Area Area { get; set; }

        public Int32 IdDepreciacionFk { get; set; }
        public Depreciacion Depreciacion { get; set; }
    }
}

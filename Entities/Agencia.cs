﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
 public  class Agencia
    {
        public Agencia()
        {
            DetallesAgencias = new HashSet<DetalleAgencia>();
        }
        public Int32 IdAgencia { get; set; }
        public string Nombre { get; set; }
        public string CodigoAgencia { get; set; }

        public ICollection<DetalleAgencia> DetallesAgencias { get; set; }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AlinWebSystem.Startup))]
namespace AlinWebSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

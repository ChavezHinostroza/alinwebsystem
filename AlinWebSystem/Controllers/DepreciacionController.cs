﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Repository;
using Interfaces;
using Validator.DepreciacionValidator;

namespace AlinWebSystem.Controllers
{
    public class DepreciacionController : Controller
    {
        private InterfaceDepreciacion repository;
        private DepreciacionValidator validator;

        public DepreciacionController(InterfaceDepreciacion repository,DepreciacionValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.AllDepreciacion("");

            return View("ListaDeprecicion", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarDeprecicion");

        }

        [HttpPost]
        public ActionResult Create(Depreciacion depreciacion)
        {
            validator.Execute(depreciacion);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                repository.Store(depreciacion);
                TempData["UpdteSuccess"] = "Se Guardo Correctamente";
                return RedirectToAction("Index");
            }

            return View("RegistrarDeprecicion", depreciacion);

        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("EditarDepreciacion", data);
        }

        [HttpPost]
        public ActionResult Edit(Depreciacion depreciacion)
        {
             validator.Execute(depreciacion);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
            repository.Update(depreciacion);
            TempData["UpdateSuccess"] = "Se Actualizo correctamente";
            return RedirectToAction("Index");
            }
            return View("EditarDepreciacion", depreciacion);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }
    }
}

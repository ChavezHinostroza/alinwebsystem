﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Interfaces;
using Entities;
using Validator.ProductoValidators;

namespace AlinWebSystem.Controllers
{
    public class ProductoController : Controller
    {
        //
        // GET: /Postulante/
        private InterfaceProducto repository;

        private ProductoValidators validator;
        //private string proveedor;




        public ProductoController(InterfaceProducto repository, ProductoValidators validator)
        {
            this.repository = repository;
            this.validator = validator;

        }



        [HttpGet]
        public ViewResult Index(string query = "", DateTime? fecha1 = null, DateTime? fecha2 = null)
        {
            // TraerProveedor();
            var datos = repository.ByQueryNameMoreDate(query, fecha1, fecha2);
            return View("ListaProductos", datos);
        }

        [HttpGet]
        public ViewResult Create(string TipoDeProducto)
        {
            
            //TraerProveedor();
            if (TipoDeProducto == "Suministros")
            {
                return View("RegistrarSuministros");
            }
            else if (TipoDeProducto == "Vestuario")
            {
                return View("RegistrarVestuarios");
            }
            else
            {
                return View("RegistrarOtros");
            }
        }

        [HttpPost]
        public ActionResult Create(Producto producto, string TipoDeProducto)
        {
            validator.Execute(producto);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (TipoDeProducto == "Suministros")
            {
                //validator.Execute(producto);
                //validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
                if (ModelState.IsValid)
                {
                    repository.Store(producto);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }

                return View("RegistrarSuministros", producto);
            }

            else if (TipoDeProducto == "Vestuario")
            {
                //validator.Execute(producto);
                //validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
                if (ModelState.IsValid)
                {
                    repository.Store(producto);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }

                return View("RegistrarVestuarios", producto);

            }
            else
            {
                //validator.Execute(producto);
                //validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
                if (ModelState.IsValid)
                {
                    repository.Store(producto);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }
                return View("RegistrarOtros", producto);
            }


            //if (ModelState.IsValid)
            //{
            //    repository.Store(producto);
            //    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
            //    return RedirectToAction("Index");
            //}

            //return View("RegistrarSuministros", producto);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("EditarProducto", data);
        }

        [HttpPost]
        public ActionResult Edit(Producto producto)
        {
            repository.Update(producto);
            TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }

    }
}

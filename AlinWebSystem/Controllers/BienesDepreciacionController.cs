﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Repository;
using Interfaces;


namespace AlinWebSystem.Controllers
{
    public class BienesDepreciacionController : Controller
    {
        private InterfaceBien repoBiene;
        private InterfaceDepreciacion repoDepreciacion;

        public BienesDepreciacionController(InterfaceBien repoBiene, InterfaceDepreciacion repoDepreciacion)
        {
            this.repoBiene = repoBiene;
            this.repoDepreciacion = repoDepreciacion;
        }

        private void CargarDepreciacion()
        {
            var depreciacion = repoDepreciacion.AllDepreciacion("");
            ViewData["IdDepreciacionFk"] = new SelectList(depreciacion, "IdDepreciacion", "Porcentaje");
        }

        [HttpGet]
        public ViewResult Index(DateTime? fecha1, DateTime? fecha2, String TipoDeProducto, string criterio = "")
        {
            CargarDepreciacion();
            var datos = repoBiene.GetBienesByfechas(criterio, fecha1, fecha2);

            return View("ListaDeDepreciacion", datos);
        }

       
    }
}

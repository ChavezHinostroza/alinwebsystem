﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Repository;
using Interfaces;
using Validator.BienesValidator;


namespace AlinWebSystem.Controllers
{
    public class BienesController : Controller
    {
        private InterfaceBien repoBienes;
        private InterfaceProducto repoProducto;
        private InterfaceAgencia repoAgencia;
        private InterfaceArea repoArea;
        private BienesValidator validator;
        private InterfaceDepreciacion repoDepreciacion;

        public BienesController(InterfaceBien repoBienes, InterfaceProducto repoProducto, InterfaceAgencia repoAgencia, InterfaceArea repoArea, BienesValidator validator, InterfaceDepreciacion repoDepreciacion)
        {
            this.repoBienes = repoBienes;
            this.repoProducto = repoProducto;
            this.repoAgencia = repoAgencia;
            this.repoArea = repoArea;
            this.repoDepreciacion = repoDepreciacion;
            this.validator = validator;
        }

        private void TableProductosYAgencia()
        {
           
            var agencia = repoAgencia.All();
            ViewData["IdAgencia"] = new SelectList(agencia, "IdAgencia", "Nombre");

            var area = repoArea.AllArea("");
            ViewData["IdAreafk"] = new SelectList(area, "IdArea", "Nombre");

            var depreciacion = repoDepreciacion.AllDepreciacion("");
            ViewData["IdDepreciacionFk"] = new SelectList(depreciacion, "IdDepreciacion", "Descripcion");

        }

        [HttpGet]
        public ViewResult Index(string query = "", DateTime? fecha1= null, DateTime? fecha2= null)
        {
            TableProductosYAgencia();
            var datos = repoBienes.GetBienesByfechas(query, fecha1, fecha2);
            
            return View("ListaDeBienes", datos);
        }

        [HttpGet]
        public ViewResult Create(string TipoDeProducto)
        {
            TableProductosYAgencia();
            if (TipoDeProducto == "Muebles")
            {
                return View("RegistroDeMuebles");
            }
            else if (TipoDeProducto == "Computo")
            {
                return View("RegistrarProducComputo");
            }
            else if (TipoDeProducto == "Vehiculos")
            {
                return View("RegistroDeVehiculos");
            }else
            {
                return View("RegistrarOtrosBienes");
            }
          
        }

        [HttpPost]
        public ActionResult Create(Bienes bienes, string TipoDeProducto)
        {
            validator.Execute(bienes);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            string segundaParteCodigo = "";

            if (TipoDeProducto == "Muebles")
            {
                if (ModelState.IsValid)
                {
                    var bienesBD = repoBienes.ObtenerBienesDeUnTipoEspecifico("Muebles");
                    if (bienesBD == null || bienesBD.Count() == 0)
                    {
                        segundaParteCodigo = segundaParteCodigo + "01";
                    }
                    else
                    {
                        Bienes bien = bienesBD.Last();
                        int n = bien.CodigoGrupo.Length;
                        string ultimoNro = bien.CodigoGrupo[n - 2] + "" + bien.CodigoGrupo[n - 1] + "";

                        int siguienteNro = int.Parse(ultimoNro) + 1;

                        string siguienteNroString = siguienteNro.ToString();
                        int newN = siguienteNroString.Length;
                        if (newN == 1)
                        {

                            siguienteNroString = "0" + siguienteNroString;
                        }
                        segundaParteCodigo = siguienteNroString;
                    }

                    repoBienes.Store(bienes);

                    string codigo = bienes.Grupo + "-" + segundaParteCodigo;
                    bienes.CodigoGrupo = codigo;

                    repoBienes.Update(bienes);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }
                TableProductosYAgencia();
                return View("RegistroDeMuebles", bienes);
            //return View("ListaDeBienes", bienes);
                //iva aca el first return  
            //return View("RegistrarBienes", bienes);
            }
            else if (TipoDeProducto == "Computo")
            {
                if (ModelState.IsValid)
                {
                    var bienesBD = repoBienes.ObtenerBienesDeUnTipoEspecifico("Computo");
                    if (bienesBD == null || bienesBD.Count() == 0)
                    {
                        segundaParteCodigo = segundaParteCodigo + "01";
                    }
                    else
                    {
                        Bienes bien = bienesBD.Last();
                        int n = bien.CodigoGrupo.Length;
                        string ultimoNro = bien.CodigoGrupo[n - 2] + "" + bien.CodigoGrupo[n - 1] + "";

                        int siguienteNro = int.Parse(ultimoNro) + 1;

                        string siguienteNroString = siguienteNro.ToString();
                        int newN = siguienteNroString.Length;
                        if (newN == 1)
                        {

                            siguienteNroString = "0" + siguienteNroString;
                        }
                        segundaParteCodigo = siguienteNroString;
                    }

                    repoBienes.Store(bienes); 
                  

                    string codigo = bienes.Grupo + "-" + segundaParteCodigo;
                    bienes.CodigoGrupo = codigo;

                    repoBienes.Update(bienes);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }
                TableProductosYAgencia();

                return View("RegistrarProducComputo", bienes);
            }
            else if (TipoDeProducto == "Vehiculos")
            {
                if (ModelState.IsValid)
                {
                    var bienesBD = repoBienes.ObtenerBienesDeUnTipoEspecifico("Vehiculos");
                    if (bienesBD == null || bienesBD.Count() == 0)
                    {
                        segundaParteCodigo = segundaParteCodigo + "01";
                    }
                    else
                    {
                        Bienes bien = bienesBD.Last();
                        int n = bien.CodigoGrupo.Length;
                        string ultimoNro = bien.CodigoGrupo[n - 2] + "" + bien.CodigoGrupo[n - 1] + "";

                        int siguienteNro = int.Parse(ultimoNro) + 1;

                        string siguienteNroString = siguienteNro.ToString();
                        int newN = siguienteNroString.Length;
                        if (newN == 1)
                        {

                            siguienteNroString = "0" + siguienteNroString;
                        }
                        segundaParteCodigo = siguienteNroString;
                    }

                    repoBienes.Store(bienes);

                    string codigo = bienes.Grupo + "-" + segundaParteCodigo;

                    bienes.CodigoGrupo = codigo;

                    repoBienes.Update(bienes);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }
                TableProductosYAgencia();

                return View("RegistroDeVehiculos", bienes);
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var bienesBD = repoBienes.ObtenerBienesDeUnTipoEspecifico("Otros");
                    if (bienesBD == null || bienesBD.Count() == 0)
                    {
                        segundaParteCodigo = segundaParteCodigo + "01";
                    }
                    else
                    {
                        Bienes bien = bienesBD.Last();
                        int n = bien.CodigoGrupo.Length;
                        string ultimoNro = bien.CodigoGrupo[n - 2] + "" + bien.CodigoGrupo[n - 1] + "";

                        int siguienteNro = int.Parse(ultimoNro) + 1;

                        string siguienteNroString = siguienteNro.ToString();
                        int newN = siguienteNroString.Length;
                        if (newN == 1)
                        {

                            siguienteNroString = "0" + siguienteNroString;
                        }
                        segundaParteCodigo = siguienteNroString;
                    }

                    repoBienes.Store(bienes);

                    string codigo = bienes.Grupo + "-" + segundaParteCodigo;
                    bienes.CodigoGrupo = codigo;

                    repoBienes.Update(bienes);
                    TempData["UpdateSuccess"] = "Se Guardo Correctamente";
                    return RedirectToAction("Index");
                }

                TableProductosYAgencia();

                return View("RegistroDeVehiculos", bienes);
            }

        }

        [HttpGet]
        public ViewResult Edit(Int32 id)
        {
            var data = repoBienes.Find(id);

           
            var agencia = repoAgencia.All();
            ViewData["IdAgencia"] = new SelectList(agencia, "IdAgencia", "Nombre", data.IdAgencia);

            var area = repoArea.AllArea("");
            ViewData["IdAreafk"] = new SelectList(area, "IdArea", "Nombre", data.IdAreafk);

            var depreciacion = repoDepreciacion.AllDepreciacion("");
            ViewData["IdDepreciacionFk"] = new SelectList(depreciacion, "IdDepreciacion", "Porcentaje");


            return View("EditarTipoBieMuebles", data);
        }

        [HttpPost]
        public ActionResult Edit(Bienes bienes)
        {
            repoBienes.Update(bienes);
            TempData["UpdateSuccess"] = "Actualizado Correctamente";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repoBienes.Delete(id);
            TempData["UpdateSuccess"] = "Se eliminó corrctamente";

            return RedirectToAction("Index");
        }

    }
}

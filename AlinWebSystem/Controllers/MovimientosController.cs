﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Interfaces;
using Entities;
using Validator.MovimientosValidators;
//using SGRondesa.ViewModels;

namespace AlinWebSystem.Controllers
{
    public class MovimientosController : Controller
    {
        private InterfaceProducto repoproducto;
        private InterfaceAgencia repoAgencia;
        private InterfaceArea repoArea;
        private InterfaceMovimientos interfaceMovimientos;
        private MovimientosValidators validator;
        private InterfaceProveedor interfaceProveedor;

        private void TraerProveedor()
        {
            var proveedor = interfaceProveedor.AllCriteriosProveedor();
            proveedor.Insert(0, new Proveedor() { IdProveedor = 0, Correo = null, RazonSocial = " [Seleccionar Proveedor] ", Representante = null, RUC = null, Telefono = null });
            ViewData["IdProveedorFk"] = new SelectList(proveedor, "IdProveedor", "RazonSocial");
        }

        public MovimientosController(InterfaceProducto repoproducto, InterfaceProveedor interfaceProveedor, InterfaceAgencia repoAgencia, InterfaceArea repoArea, InterfaceMovimientos interfaceMovimientos, MovimientosValidators validator)
        {
            this.repoproducto = repoproducto;
            this.interfaceMovimientos = interfaceMovimientos;
            this.validator = validator;
            this.repoArea = repoArea;
            this.repoAgencia = repoAgencia;
            this.interfaceProveedor = interfaceProveedor;

        }

        private void TraerProductoAreasAgencia()
        {
            var producto = repoproducto.AllProducto("");
            producto.Insert(0, new Producto() { IdProducto = 0, Nombre = " [Seleccionar Producto] ", FechaIngreso = DateTime.Now, Precio = 1, UnidadMedida = null, Observaciones = null });
            ViewData["IdProductofk"] = new SelectList(producto, "IdProducto", "Nombre");

            var agencia = repoAgencia.All();
            agencia.Insert(0, new Agencia() { IdAgencia = 0, Nombre = " [Seleccionar Agencia] ", CodigoAgencia = null });
            ViewData["IdAgencia"] = new SelectList(agencia, "IdAgencia", "Nombre");

            var area = repoArea.AllArea("");
            area.Insert(0, new Area() { IdArea = 0, Nombre = " [Seleccionar Area] ", CodigoArea = null });
            ViewData["IdAreafk"] = new SelectList(area, "IdArea", "Nombre");

        }

        [HttpGet]
        public ViewResult Index(string query = "", DateTime? fecha1 = null, DateTime? fecha2 = null)
        {

            // TraerProducto();
            var datos = interfaceMovimientos.ByQueryMoreDate(query, fecha1, fecha2);

            return View("Movimientos", datos);


        }

        [HttpGet]
        public ViewResult Create()
        {
            TraerProveedor();
            TraerProductoAreasAgencia();
            return View("RegistrarMovimientos");

        }


        [HttpPost]
        public ActionResult Create(Movimientos movimientos, int IdProducto, int Stock)
        {
           

            TraerProveedor();
            TraerProductoAreasAgencia();
            var producto = interfaceMovimientos.obtenerProoductoById(IdProducto);

            if (movimientos.Tipo == "Entrada")
            {
                producto.Cantidad = producto.Cantidad + Stock;
            }
            else if (movimientos.Tipo == "Salida")
            {
                producto.Cantidad = producto.Cantidad - Stock;
            }

            repoproducto.Update(producto);

            validator.Execute(movimientos);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                movimientos.IdProductoFk = IdProducto;
                interfaceMovimientos.Store(movimientos);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }
            return View("RegistrarMovimientos", movimientos);


        }

        [HttpGet]
        public ViewResult TraerProducto(int id)
        {
            TraerProveedor();
            TraerProductoAreasAgencia();
            var producto = interfaceMovimientos.obtenerProoductoById(id);
            //var producto = new Producto() { 


            //};

            return View("RegistrarMovimientos", producto);
        }


    }
}

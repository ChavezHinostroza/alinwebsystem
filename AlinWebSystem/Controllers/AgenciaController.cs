﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Entities;
using Interfaces;
using Validator.AgenciaValidator;
using DataBase;
using Repository;

namespace AlinWebSystem.Controllers
{
    public class AgenciaController : Controller
    {
        private InterfaceAgencia repoAgencia;
        private InterfaceArea repoAreas;
        private InterfaceDetalleAgencia detalleAgencia;
        private AgenciaValidator validator;

        public AgenciaController(AgenciaRepository repoAgencia, AreaRepository repoAreas, DetalleAgenciaRepository detalleAgencia,AgenciaValidator validator)
        {
            this.repoAgencia = repoAgencia;
            this.repoAreas = repoAreas;
            this.detalleAgencia = detalleAgencia;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string criterio = "")
        {
            var datos = repoAgencia.ByQueryAll(criterio);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            ViewBag.Area = repoAreas.AllArea("");
            return View("RegistrarAgencia");

        }

        [HttpPost]
        public ActionResult Create(Agencia agencia)
        {
            //validator.Execute(agencia);
            //validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (agencia == null)
            {
                agencia = new Agencia();
            }
            if (ModelState.IsValid)
            {
                repoAgencia.Store(agencia);
                TempData["UpdateSuccess"] = "Se Guardo Correctmente";
                return RedirectToAction("Index");
            }
            return View("RegistrarAgencia", agencia);
        }

        [HttpGet]
        public ActionResult Item(int id, int position)
        {
            var model = repoAreas.Find(id);
            ViewBag.Area = position;
            return PartialView("_Item", model);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repoAgencia.Find(id);
            ViewBag.Detalle = detalleAgencia.GetDetalleByNro(data.Nombre);
            //ViewBag.Area = repoAreas.All();
            return View("Editar", data);
        }

        [HttpPost]
        public ActionResult Edit(Agencia agencia)
        {
             validator.Execute(agencia);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
            if (ModelState.IsValid)
                {

                    repoAgencia.Update(agencia);
                    TempData["UpdateSuccess"] = "Se Actualizo correctamente";
                    return RedirectToAction("Index");
                }
            return View("Editar", agencia);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            repoAgencia.Delete(id);
            TempData["UpdateSuccess"] = "Se Elimino correctamente";
            return RedirectToAction("Index");
        }
    }
}

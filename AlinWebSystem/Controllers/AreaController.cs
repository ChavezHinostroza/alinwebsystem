﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Interfaces;
using Entities;
using Validator.AreaValidator;
using Repository;

namespace AlinWebSystem.Controllers
{
    public class AreaController : Controller
    {
        private InterfaceArea areaRepository;
         private AreaValidator validator;

         public AreaController(AreaRepository areaRepository, AreaValidator validator)
        {
            this.areaRepository = areaRepository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = areaRepository.AllArea(query);
            return View("Inicio", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarArea");
        }

        [HttpPost]
        public ActionResult Create(Area area)
        {
            validator.Execute(area);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                areaRepository.Store(area);
                TempData["UpdateSuccess"] = "Se actualizo correctamente";
                return RedirectToAction("Index");
            }

            return View("RegistrarArea", area);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = areaRepository.Find(id);
            return View("EditarArea", data);
        }

        [HttpPost]
        public ActionResult Edit(Area area)
        {
            validator.Execute(area);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
            if (ModelState.IsValid)
            {
                areaRepository.Update(area);
                TempData["UpdateSuccess"] = "Se Actualizo correctamente";
                return RedirectToAction("Index");
            }
             return View("EditarArea", area);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            areaRepository.Delete(id);
            TempData["UpdateSuccess"] = "Se Élimino correctamente";
            return RedirectToAction("Index");
        }
    }
}

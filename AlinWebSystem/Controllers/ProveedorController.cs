﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

//Importamos
using Interfaces;
using Entities;
using Validator.ProveedorValidators;

namespace AlinWebSystem.Controllers
{
    public class ProveedorController : Controller
    {
        private InterfaceProveedor repository;
        private ProveedorValidators validator;

        public ProveedorController(InterfaceProveedor repository, ProveedorValidators validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        [HttpGet]
        public ViewResult Index(string query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("ListaProveedores", datos);
        }

        [HttpGet]
        public ViewResult Create()
        {
            return View("RegistrarProveedor");
        }

        [HttpPost]
        public ActionResult Create(Proveedor proveedor)
        {
            validator.Execute(proveedor);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));

            if (ModelState.IsValid)
            {
                repository.Store(proveedor);

                TempData["UpdateSuccess"] = "Se Guardo Correctamente";

                return RedirectToAction("Index");
            }

            return View("RegistrarProveedor", proveedor);
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            var data = repository.Find(id);
            return View("EditarProveedor", data);
        }

        [HttpPost]
        public ActionResult Edit(Proveedor proveedor)
        {
            validator.Execute(proveedor);
            validator.errors.ToList().ForEach(x => ModelState.AddModelError(x.Key, x.Value));
            if (ModelState.IsValid)
            {
                repository.Update(proveedor);
                TempData["UpdateSuccess"] = "Se Actualizó Correctamente";
                return RedirectToAction("Index");
            }
            return View("EditarProveedor",proveedor);
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            repository.Delete(id);
            TempData["UpdateSuccess"] = "Se Eliminó Correctamente";
            return RedirectToAction("Index");
        }
    }
}
